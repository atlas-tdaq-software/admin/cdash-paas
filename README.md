# CDashboard on OpenStack

These modifications and instructions allow to run CDash
on Openstack.

## All-in-one script

Run the `make-cdash.sh` script in this directory passing it the desired hostname of your cdashboard.
The initial name component is used as the project name. 

```shell
make-cdash.sh my-cdash.app.cern.ch
```

Now point your browser to `https://my-cdash.app.cern.ch` to do the initial installation. 

## Manual Installation

Replace `my-cdash` in  the following with the name you want
your website to have.

First create a new project.

```shell
oc new-project my-cdash --description="CDashboard"
```

We need a MySQL server with persistent memory. We use
the `mysql-persistent` template if it is available.

Check the supported parameters:

```shell
oc process --parameters -n openshift mysql-persistent
```

We are going to set `MYSQL_USER` and `MYSQL_DATABASE` parameters. If
you know how much space you need, you can also set 
`VOLUME_CAPACITY`, although you can always increase
that later.

Now let's create the database service from the template:

```shell
oc new-app --template=openshift/mysql-persistent -p MYSQL_USER=cdash -p MYSQL_DATABASE=cdash
```

We need the generated password for our database:

```shell
pw=$(oc get secret mysql --template='{{index .data "database-password"}}' | base64 -d)
```

Next we build the CDash application: choose the initial admin password here instead of `SECRET`.

```shell
oc new-app https://gitlab.cern.ch/atlas-tdaq-software/admin/cdash-paas.git -e CDASH_ROOT_ADMIN_PASS=SECRET -e CDASH_CONFIG='$CDASH_DB_LOGIN = "cdash"; $CDASH_DB_HOST = "mysql"; $CDASH_DB_PASS = "'${pw}'";  $CDASH_ASSET_URL = "https://my-cdash.app.cern.ch";  $CDASH_BASE_URL = "https://'$1'"; '

```

```shell
oc create route edge --service cdash-paas --insecure-policy=Redirect --hostname my-cdash.app.cern.ch
```

To make the site available outside of CERN:

```shell
oc annotate --overwrite route/cdash-paas haproxy.router.openshift.io/ip_whitelist=""
```

Now point your browser to `https://my-cdash.app.cern.ch` to do the initial installation. 

